import React from 'react';
import Hello from './component.jsx';

main();

function main() {

  // We make a new promise: we promise the string 'result' (after waiting 3s)
  var p1 = new Promise(
    // The resolver function is called with the ability to resolve or
    // reject the promise
    function(resolve, reject) {
        // This is only an example to create asynchronism
        window.setTimeout(
            function() {
                // We fulfill the promise !
                resolve(0);
            }, Math.random() * 2000 + 1000);
    });

  Promise.resolve(p1);

  p1.then(() => {
    React.render(<Hello />, document.getElementById('app'));
  });
}
