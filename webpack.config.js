var path = require('path');

var config = {
    entry: [
      'webpack/hot/dev-server',
      'webpack-dev-server/client?http://localhost:8080',
      path.resolve(__dirname, 'app/main.js')
    ],
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js',
    },
    module: {
      loaders: [{
        test: /\.jsx?$/, // A regexp to test the require path. accepts either js or jsx
        loader: 'babel-loader', // The module to load. "babel" is short for "babel-loader"
        exclude: '/node_modules/',
        query: {
          presets: ['es2015', 'react']
        }
      },{ 
        test: /\.js$/, 
        exclude: /node_modules/, 
        loader: "babel", 
        query: {
          presets:['es2015', 'react']
        }
      }]
    }
};

module.exports = config;